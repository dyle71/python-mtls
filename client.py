import pprint
import socket
import ssl

# Based on
# https://stackoverflow.com/questions/44343230/mutual-ssl-authentication-in-simple-echo-client-server-python-sockets-ssl-m


if __name__ == '__main__':

    # SSL Client with mTLS

    HOST = '127.0.0.1'
    PORT = 1234

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setblocking(1)
    sock.connect((HOST, PORT))

    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.verify_mode = ssl.CERT_REQUIRED
    context.load_verify_locations('crt/server.pem')
    context.load_cert_chain(certfile="crt/client.pem", keyfile="crt/client.key")

    if ssl.HAS_SNI:
        secure_sock = context.wrap_socket(sock, server_side=False, server_hostname=HOST)
    else:
        secure_sock = context.wrap_socket(sock, server_side=False)

    cert = secure_sock.getpeercert()
    print(cert)

    # verify server
    if cert is None:
        raise RuntimeError('No cert received')
    
    pprint.pprint(cert)
    
    cn = None
    for subject_entry in cert['subject']:
        for (subject, value) in subject_entry:
            if subject == 'commonName':
                cn = value
                break
        if cn is not None:
            break

    if cn is None:
        raise RuntimeError('No commonName in certificate.')
    print(f"commonName of server: '{cn}'")

    secure_sock.write('hello'.encode('utf-8'))
    print(secure_sock.read(1024))

    secure_sock.close()
    sock.close()
