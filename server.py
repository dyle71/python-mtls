import socket
import ssl
import pprint

# Based on
# https://stackoverflow.com/questions/44343230/mutual-ssl-authentication-in-simple-echo-client-server-python-sockets-ssl-m


if __name__ == '__main__':

    # SSL Server with mTLS

    HOST = '127.0.0.1'
    PORT = 1234

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen(10)

    client, fromaddr = server_socket.accept()
    secure_sock = ssl.wrap_socket(client,
                                  server_side=True,
                                  ca_certs="crt/client.pem",
                                  certfile="crt/server.pem",
                                  keyfile="crt/server.key",
                                  cert_reqs=ssl.CERT_REQUIRED,
                                  ssl_version=ssl.PROTOCOL_TLSv1_2)

    print(repr(secure_sock.getpeername()))
    print(secure_sock.cipher())
    print(pprint.pformat(secure_sock.getpeercert()))
    cert = secure_sock.getpeercert()

    # verify client
    if cert is None:
        raise RuntimeError('No cert received')

    pprint.pprint(cert)

    cn = None
    for subject_entry in cert['subject']:
        for (subject, value) in subject_entry:
            if subject == 'commonName':
                cn = value
                break
        if cn is not None:
            break

    if cn is None:
        raise RuntimeError('No commonName in certificate.')
    print(f"commonName of client: '{cn}'")

    try:
        data = secure_sock.read(1024)
        secure_sock.write(data)
    finally:
        secure_sock.close()
        server_socket.close()
