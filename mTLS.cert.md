# How to create the self-signed certs

## Client

1. Create the client private key:
```bash
$ openssl genrsa -out client.key 4096
```

2. Create Certificate
```bash
$ openssl req -x509 -key client.key -out client.pem -days 3650 -subj '/C=DE/ST=Bavaria/L=Nuernberg/emailAddress=alice@keequant.com/O=KEEQuant GmbH/OU=Office/CN=alice'
```

3. Optional: view certificate
```bash
$ openssl x509 -noout -text -in client.pem 
```


## Server

1. Create the server private key:
```bash
$ openssl genrsa -out server.key 4096
```

2. Create Certificate
```bash
$ openssl req -verbose -x509 -key server.key -out server.pem -days 3650 -subj '/CN=bob.server.example'
```

3. Optional: view certificate
```bash
$ openssl x509 -noout -text -in server.pem 
```

## Utilities

* Show public key from a server (or client resp.) PEM cert file:
```bash
$ openssl x509 -in crt/server.pem -pubkey -noout
```

* Show public key from a server (or client resp.) KEY file:
```bash
$ openssl rsa -in crt/server.key -pubout
```

* Show private key from a server (or client resp.) KEY file:
```bash
$ openssl rsa -in crt/server.key
```
